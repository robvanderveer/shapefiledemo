//
//  CALayer+additions.h
//  SimplyStats
//
//  Created by Rob van der Veer on 7/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/CALayer.h>
@interface CALayer (Additions)
- (void)moveToFront;
- (UIImage *)getImageWithSize:(CGSize)size opaque:(bool)opaque contentScale:(CGFloat)scale;
- (UIImage *)getImageFromLayer;
@end