//
//  ShapeFileLayer.m
//  ShapeFileDemo
//
//  Created by Rob van der Veer on 5/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "ShapeFileLayer.h"

@implementation ShapeFileLayer

-(id)init
{
    self = [super init];
    if(self)
    {
        self.shouldRasterize = YES;
    }
    return self;
}

+ (Class)layerClass
{
	return [CAShapeLayer class];
}

-(id)initWithLayer:(id)layer
{
    self = [super initWithLayer:layer];
    if(self)
    {
        ShapeFileLayer *other = layer;
      
        self.shape = other.shape;
        self.frame = other.frame;
        self.path = other.path;
        self.isSelected = other.isSelected;
        self.isHighlighted = other.isHighlighted;
        self.value = other.value;
        self.shapeBorderColor = other.shapeBorderColor;
        self.shouldRasterize = other.shouldRasterize;
        self.rasterizationScale = other.rasterizationScale;
        self.contentsScale = other.contentsScale;
        self.lineWidth = other.lineWidth;
        self.masksToBounds = other.masksToBounds;
        self.mask = other.mask;
    }
    return self;
}

-(id<CAAction>)actionForKey:(NSString *)event
{
    if([event isEqualToString:@"lineWidth"] ||
        [event isEqualToString:@"fillColor"] ||
        [event isEqualToString:@"opacity"] ||
       [event isEqualToString:@"strokeColor"])
    {
        CABasicAnimation *ani = [CABasicAnimation animationWithKeyPath:event]; // Default Animation for 'event'
        ani.fromValue = [self valueForKey:event];
        ani.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
        ani.delegate = self;
        //ani.duration = 1;
        return ani;
    }
    return [super actionForKey:event];
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    //NSLog(@"Animation complete");
}

-(void)setIsHighlighted:(bool)isHighlighted
{
    _isHighlighted = isHighlighted;
    [self updateMe];
}

-(void)setIsSelected:(bool)isSelected
{
    _isSelected = isSelected;
    [self updateMe];
}

-(void)updateMe
{
    if(_isSelected)
    {
        if(_isHighlighted)  //hovering.
        {
            [self setLineWidth:2/[self contentsScale]];
            [self setFillColor:[UIColor colorWithWhite:1 alpha:_value].CGColor];
            [self setStrokeColor:[UIColor colorWithWhite:1 alpha:1].CGColor];
        }
        else
        {
            [self setLineWidth:1/[self contentsScale]];
            [self setFillColor:[UIColor colorWithWhite:1 alpha:_value].CGColor];
            [self setStrokeColor:[UIColor colorWithWhite:1 alpha:0.5].CGColor];
        }
    }
    else
    {
        if(_isHighlighted) //hovering but inactive.
        {
            [self setLineWidth:2/[self contentsScale]];
            [self setFillColor:[UIColor colorWithWhite:0 alpha:0.4].CGColor];
            [self setStrokeColor:[UIColor colorWithWhite:1 alpha:0.5].CGColor];
        }
        else
        {
            [self setLineWidth:1/[self contentsScale]];
            [self setFillColor:[UIColor colorWithWhite:0 alpha:0.1].CGColor];
            [self setStrokeColor:self.shapeBorderColor.CGColor];
        }
    }
}


@end
