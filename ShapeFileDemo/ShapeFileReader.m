//
//  ShapeFileReader.m
//  ShapeFileDemo
//
//  Created by Rob van der Veer on 4/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "ShapeFileReader.h"
#import "shp.h"
#import "dbf.h"

#define D_R (M_PI / 180.0)
#define R_D (180.0 / M_PI)
#define R_MAJOR 6378137.0
#define R_MINOR 6356752.3142
#define RATIO (R_MINOR/R_MAJOR)
#define ECCENT (sqrt(1.0 - (RATIO * RATIO)))
#define COM (0.5 * ECCENT)

static double deg_rad (double ang) {
    return ang * D_R;
}

static double project_d(double latIn)
{
    latIn = -latIn;
    double lat = fmin (89.5, fmax (latIn, -89.5));
    double y = log(tanf((90 + lat) * M_PI / 360)) / (M_PI / 180);
    return y;
    
    return latIn;
    
//    double lat = fmin (89.5, fmax (latIn, -89.5));
//    double phi = deg_rad(lat);
//    double sinphi = sin(phi);
//    double con = ECCENT * sinphi;
//    con = pow((1.0 - con) / (1.0 + con), COM);
//    double ts = tan(0.5 * (M_PI * 0.5 - phi)) / con;
//    return 0 - 50 * log(ts);
};

@implementation Shape

@end


@implementation ShapeFileReader
{
    shp *shpReader;
    dbf *dbfReader;
    
    NSMutableArray *shapes;
}

-(ShapeFileReader *)initWithBaseName:(NSString *)filename
{
    self = [super init];
    if(self)
    {
        NSDate * start = [NSDate date];
        
        NSString *shpName = [[NSBundle mainBundle] pathForResource:filename ofType:@"shp"];
        NSString *dbfName = [[NSBundle mainBundle] pathForResource:filename ofType:@"dbf"];
        
        shpReader = [[shp alloc] init];
        [shpReader open:shpName];
    
        dbfReader = [[dbf alloc] init];
        [dbfReader open:dbfName];
      
        //we will construct our own bounding boxes because we project our records.

        shapes = [[NSMutableArray alloc] init];
        CGRect projectedRect = [self projectRect:shpReader.boundingBox];
        CGAffineTransform toZero = CGAffineTransformMakeTranslation(-projectedRect.origin.x, -projectedRect.origin.y);
        
        //read all records
        while(![shpReader eof])
        {
            shpShape rawShape = [shpReader readShape];
            
            Shape *item = [[Shape alloc] init];
            shpBoundingBox shapeBox = rawShape.boundingBox;
            
            CGRect projectedItemRect = [self projectRect:shapeBox];
            CGRect itemRect = CGRectApplyAffineTransform(projectedItemRect, toZero);
            
            CGAffineTransform toShape = CGAffineTransformTranslate(toZero, -itemRect.origin.x, -itemRect.origin.y);
            
            item.rect = itemRect;
            item.metadata = [dbfReader getRecord];
            item.shapes = [[NSMutableArray alloc] init];

            //split into shapes.
            for(int i = 0; i < rawShape.numParts; i++)
            {
                NSMutableArray *points = [[NSMutableArray alloc] init];
                
                int max = (i < rawShape.numParts-1)?rawShape.parts[i+1]:rawShape.numPoints;
                for(int pointCount = rawShape.parts[i]; pointCount < max; pointCount++)
                {
                    shpPoint rawP = rawShape.points[pointCount];
                    assert(rawP.x >= shapeBox.minX);
                    assert(rawP.x <= shapeBox.maxX);
                    assert(rawP.y >= shapeBox.minY);
                    assert(rawP.y <= shapeBox.maxY);
                    
                    CGPoint p = [self projectPoint:rawP];
                    //assert(CGRectContainsPoint(itemRect, p));
 
                    //coordinates should be relative to its own bounding box.
                    CGPoint p3 = CGPointApplyAffineTransform(p, toShape);
 
                    [points addObject:[NSValue valueWithCGPoint:p3]];
                }
                [item.shapes addObject:points];
            }
            
            [shapes addObject:item];
            [shpReader freeShape:rawShape];
                    }
        [shpReader close];
        [dbfReader close];
        
        //set up the rectangle we want to see.
        shpBoundingBox worldBox;
        worldBox.minX = -180;
        worldBox.maxX = 180;
        worldBox.minY = -84;
        worldBox.maxY = 75;
        _rect = [self projectRect:worldBox];
        
        NSLog(@"loading the world took: %f", -[start timeIntervalSinceNow]);
    }
    return self;
}

-(CGRect)projectRect:(shpBoundingBox)box
{
    CGFloat y1 = project_d(box.minY);
    CGFloat y2 = project_d(box.maxY);
    if(y1 > y2)
    {
        //swap
        CGFloat tmp = y1; y1 = y2; y2 = tmp;
    }
    return CGRectMake(box.minX, y1, box.maxX-box.minX, y2 - y1);
}

-(CGPoint)projectPoint:(shpPoint)p
{
    CGFloat y = project_d(p.y);
    
    return CGPointMake(p.x, y);
}

-(int)shapeCount
{
    return shapes.count;
}

-(Shape *)getShape:(int)index
{
    return shapes[index];
}
@end
