//
//  AppDelegate.h
//  ShapeFileDemo
//
//  Created by Rob van der Veer on 4/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
