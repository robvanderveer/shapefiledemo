//
//  ViewController.m
//  ShapeFileDemo
//
//  Created by Rob van der Veer on 4/6/13.
//  copied from:
//  http://www.raywenderlich.com/10518/how-to-use-uiscrollview-to-scroll-and-zoom-content
//

#import "ViewController.h"
#import "ShapeFileView.h"
#import "ShapeFileLayer.h"
#import <QuartzCore/QuartzCore.h>

@implementation ViewController

-(void)viewDidLoad
{
    ShapeFileReader *reader = [[ShapeFileReader alloc] initWithBaseName:@"ne_110m_admin_0_countries"];
    
    //1. Create the mapview and fit/set the desired size.
    self.myMapView = [[ShapeFileView alloc] initWithFrame:self.myScrollView.bounds andReader:reader];
    CGSize size = [self.myMapView sizeThatFits:self.view.bounds.size];
    
    self.myMapView.frame = CGRectMake(0, 0, size.width, size.height);
    self.myMapView.backgroundColor = self.view.backgroundColor;
    self.myMapView.shapeBorderColor = self.view.backgroundColor;
    
    //2. Move the mapview into the scrollviewer
    [self.myScrollView addSubview:self.myMapView];
    self.myScrollView.contentSize = self.myMapView.bounds.size;

    UITapGestureRecognizer *singleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewSingleFingerTapped:)];
    singleTapRecognizer.numberOfTapsRequired = 2;
    singleTapRecognizer.numberOfTouchesRequired = 1;    //only one finger required.
    [self.myScrollView addGestureRecognizer:singleTapRecognizer];
    
    //3. Add a recognizer so scale the map.
    UITapGestureRecognizer *twoFingerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTwoFingerTapped:)];
    twoFingerTapRecognizer.numberOfTapsRequired = 1;
    twoFingerTapRecognizer.numberOfTouchesRequired = 2;
    [self.myScrollView addGestureRecognizer:twoFingerTapRecognizer];
    
}

-(void)didReceiveMemoryWarning
{
    NSLog(@"MEMORY WARNING");
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.myMapView rebuild];
    
    [self updateZoomScale];
    [self centerScrollViewContents];

    //warning: this data gets destroyed after resizing.
    ShapeFileLayer *layer = [self.myMapView getLayerForCountry:@"Netherlands"];
    [layer setIsSelected:true];
    ShapeFileLayer *layer2 = [self.myMapView getLayerForCountry:@"United States"];
    [layer2 setIsSelected:true];
    
    [self.myMapView setNeedsDisplay];
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    CGSize size = [_myMapView sizeThatFits:_myScrollView.bounds.size];
    
    [_myScrollView setContentSize:size];    
    [self centerScrollViewContents];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)updateZoomScale
{
    //determine the minimum zoom so the map will be not smaller than the scrollview
    CGRect scrollViewFrame = self.myScrollView.frame;
    CGFloat scaleWidth = scrollViewFrame.size.width / self.myScrollView.contentSize.width;
    CGFloat scaleHeight = scrollViewFrame.size.height / self.myScrollView.contentSize.height;
    CGFloat minScale = MIN(scaleWidth, scaleHeight);
    
    self.myScrollView.minimumZoomScale = minScale;
    self.myScrollView.maximumZoomScale = 4.0f;
    self.myScrollView.zoomScale = minScale;
    NSLog(@"minimum zoom scale %f", minScale);
}

- (void)centerScrollViewContents {
    NSLog(@"centerScrollViewContents");
    
    CGSize boundsSize = self.myScrollView.bounds.size;
    CGRect contentsFrame = self.myMapView.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    self.myMapView.frame = contentsFrame;
}

- (void)scrollViewSingleFingerTapped:(UITapGestureRecognizer*)recognizer
{
    CGFloat newZoomScale = self.myScrollView.zoomScale * 1.5f;
    newZoomScale = MIN(newZoomScale, self.myScrollView.maximumZoomScale);
    [self.myScrollView setZoomScale:newZoomScale animated:YES];
}

- (void)scrollViewTwoFingerTapped:(UITapGestureRecognizer*)recognizer
{
    // Zoom out slightly, capping at the minimum zoom scale specified by the scroll view
    CGFloat newZoomScale = self.myScrollView.zoomScale / 1.5f;
    newZoomScale = MAX(newZoomScale, self.myScrollView.minimumZoomScale);
    [self.myScrollView setZoomScale:newZoomScale animated:YES];
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    // Return the view that you want to zoom
    NSLog(@"viewForZoomingInScrollView");
    return self.myMapView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // The scroll view has zoomed, so you need to re-center the contents
    [self centerScrollViewContents];
}

-(void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    NSLog(@"scrollViewDidEndZooming %f", scale);
    
    _myMapView.contentScaleFactor = scale * [[UIScreen mainScreen] scale];
}
@end
