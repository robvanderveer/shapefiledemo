//
//  ShapeFileLayer.h
//  ShapeFileDemo
//
//  Created by Rob van der Veer on 5/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ShapeFileReader.h"

@class ShapeFileLayer;

@protocol ShapeFileLayerMoved
-(void)ShapeLayerDidBecomeActive:(ShapeFileLayer *)layer;
-(void)ShapeLayerDidDissappear:(ShapeFileLayer *)layer;
@end

@interface ShapeFileLayer : CAShapeLayer
@property (nonatomic, strong) Shape *shape;
@property (nonatomic) bool isSelected;
@property (nonatomic) bool isHighlighted;
@property (nonatomic) CGFloat value;
@property (nonatomic, strong) UIColor *shapeBorderColor;
@end
