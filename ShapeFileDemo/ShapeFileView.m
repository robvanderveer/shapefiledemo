//
//  ShapeFileView.m
//  ShapeFileDemo
//
//  Created by Rob van der Veer on 5/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ShapeFileView.h"
#import "ShapeFileLayer.h"
#import "CALayer+additions.h"
#import "NSObject+blocks.h"

@implementation ShapeFileView
{
    ShapeFileLayer *activeLayer;
    
    UIView *worldView;      //a view that is never visible.
    UIView *dynamicView;    //animating layers live here.
    
    bool isDragging;
}

-(void)initAll:(ShapeFileReader *)withReader
{
    _reader = withReader;
    
    self.shapeBorderColor = [UIColor redColor];
    self.backgroundColor = [UIColor clearColor];
    self.contentMode = UIViewContentModeRedraw;
    self.autoresizingMask = UIViewAutoresizingNone;
    self.autoresizesSubviews = false;
    
    worldView = [[UIView alloc] initWithFrame:self.bounds];
    dynamicView = [[UIView alloc] initWithFrame:self.bounds];
    
    //do not add the world, ONLY add the dynamic view
    //[self addSubview:worldView];
    [self addSubview:dynamicView];
}

- (id)initWithFrame:(CGRect)frame andReader:(ShapeFileReader *)withReader
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initAll:withReader];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initAll:NULL];
    }
    return self;
}
-(CGSize)sizeThatFits:(CGSize)size
{
    CGRect projectRect = [_reader rect];
    
    double xScale = size.width / projectRect.size.width;
    double yScale = size.height / projectRect.size.height;
    double scale = MIN(xScale,yScale);
    
    double w = floor(projectRect.size.width * scale);
    double h = floor(projectRect.size.height * scale);
    
    return CGSizeMake(w, h);
}

-(void)rebuild
{
    NSLog(@"Rebuilding the world");
    
    [[worldView layer] setSublayers:[[NSArray alloc] init]];
    [[dynamicView layer] setSublayers:[[NSArray alloc] init]];
    [self createWorldMap:self.bounds.size];
}

-(void)setContentScaleFactor:(CGFloat)scale
{
    [super setContentScaleFactor:scale];
    
    [worldView.layer setContentsScale:scale];
    [worldView.layer setContentsScale:scale];
    [dynamicView.layer setRasterizationScale:scale];
    [dynamicView.layer setRasterizationScale:scale];
    [dynamicView setContentScaleFactor:scale];

    //when rerendering, ALL world layers should be visible and on the world layer.
    //since we clone and remove hilight layers, this is never a problem.
    //Race condition when an item is added or removed
    //while zooming. just use a copy of the array.
    //but there can be only one active layer.
    if(activeLayer)
    {
        [activeLayer setRasterizationScale:scale];
        [activeLayer setContentsScale:scale];
        [activeLayer setLineWidth:1/scale];
    }
    
    for(CAShapeLayer *subLayer in worldView.layer.sublayers)
    {
        [subLayer setRasterizationScale:scale];
        [subLayer setContentsScale:scale];
        [subLayer setLineWidth:1/scale];
    }
    [self updateSnapshot];
}

-(void)createWorldMap:(CGSize)size
{
    NSDate * start = [NSDate date];
    
    CGSize desiredSize = [self sizeThatFits:size];
    CGRect projectRect = [_reader rect];
    
    double scale = desiredSize.width / projectRect.size.width;
    
    CGAffineTransform t = CGAffineTransformIdentity;
    t = CGAffineTransformScale(t, scale, scale);
    
    dynamicView.frame = CGRectMake(0, 0, desiredSize.width, desiredSize.height);
    worldView.frame = dynamicView.frame;
    
    [worldView setClipsToBounds:YES];
    [worldView setBackgroundColor:self.backgroundColor];
    [worldView.layer setOpaque:YES];
    [worldView.layer setMasksToBounds:YES];
    [worldView.layer setContentsScale:[[UIScreen mainScreen] scale]];
    [worldView.layer setRasterizationScale:[[UIScreen mainScreen] scale]];
    [worldView.layer setShouldRasterize:YES];
    [worldView.layer setBackgroundColor:self.backgroundColor.CGColor];
    
    [dynamicView setClipsToBounds:YES];
    [dynamicView.layer setContentsScale:[[UIScreen mainScreen] scale]];
    [dynamicView.layer setRasterizationScale:[[UIScreen mainScreen] scale]];
    [dynamicView.layer setOpaque:NO];
    [dynamicView.layer setShouldRasterize:YES];
    
    //lets go.
    int count = [_reader shapeCount];
    for(int shapeIndex=0;shapeIndex<count;shapeIndex++)
    {
        Shape *shape = [_reader getShape:shapeIndex];
        
        ShapeFileLayer *layer = [ShapeFileLayer layer];
        
        [layer setShape:shape];
        
        CGRect shapeRect = CGRectApplyAffineTransform(shape.rect,t);
        shapeRect = CGRectIntersection(shapeRect, dynamicView.frame);
        
        [layer setFrame:shapeRect];
        [layer setShapeBorderColor:self.shapeBorderColor];
        [layer setIsHighlighted:false];
        [layer setIsSelected:false];
        [layer setShouldRasterize:YES];
        [layer setOpaque:NO];
        
        //test
        //[layer setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.1].CGColor];

        CGMutablePathRef patch = CGPathCreateMutable();
        
        for(NSArray *pointList in shape.shapes)
        {
            for(int j = 0; j < pointList.count; j++)
            {
                CGPoint pRaw = [pointList[j] CGPointValue];
                pRaw = CGPointApplyAffineTransform(pRaw, t);
                
                /* project p */
                double x = pRaw.x;
                double y = pRaw.y;
                
                if(j == 0)
                {
                    CGPathMoveToPoint(patch, NULL, x, y);
                }
                else
                {
                    CGPathAddLineToPoint(patch, NULL, x, y);
                }
            }
        }

        [layer setContentsScale:[UIScreen mainScreen].scale];
        [layer setRasterizationScale:[UIScreen mainScreen].scale];
        [layer setPath:patch];
        CGPathRelease(patch);
        
        [worldView.layer addSublayer:layer];
    }
    
    NSLog(@"rendering the world took: %f", -[start timeIntervalSinceNow]);
    [self updateSnapshot];
}

-(void)drawRect:(CGRect)rect
{
    NSDate * start = [NSDate date];
    
    [self updateSnapshot];
    NSLog(@"Drawrect took: %f", -[start timeIntervalSinceNow]);
    
}

-(void)updateSnapshot
{
    //when we are done, build an image from the layer.
    UIImage *copy = [worldView.layer getImageWithSize:dynamicView.layer.bounds.size opaque:YES contentScale:dynamicView.layer.contentsScale];
    NSLog(@"image buffer size: %f x %f (%f)", copy.size.width, copy.size.height, copy.scale);
    
    [dynamicView.layer setContents:(id)copy.CGImage];
}

-(ShapeFileLayer *)getLayerForCountry:(NSString *)countryName
{
    //find the sublayer with the same tag.
    for (id sublayer in worldView.layer.sublayers)
    {
        if ([sublayer isKindOfClass:[ShapeFileLayer class]])
        {
            ShapeFileLayer *shapeLayer = sublayer;
            
            NSString *name = [shapeLayer.shape.metadata valueForKey:@"name"];
            
            if([name isEqualToString:countryName])
            {
                return shapeLayer;
            }
        }
    }
    return NULL;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    isDragging = false;
    [self updateShapeLayer:touches];
}

-(void)updateShapeLayer:(NSSet *)touches
{
    for(UITouch *touch in touches)
    {
        /* usually, the dynamic layer has only one sublayer */
        CGPoint touchLocation = [touch locationInView:dynamicView];
        for (id sublayer in dynamicView.layer.sublayers) {
            if ([sublayer isKindOfClass:[ShapeFileLayer class]]) {
                CGPoint layerPoint = [[dynamicView layer] convertPoint:touchLocation toLayer:sublayer];
                ShapeFileLayer *shapeLayer = sublayer;
                if (CGPathContainsPoint(shapeLayer.path, 0, layerPoint, YES)) {
                    [self updateActive:shapeLayer];
                    return;
                }
            }
        }
        
        //touchLocation = [touch locationInView:copyView];
        for (id sublayer in worldView.layer.sublayers) {
            if ([sublayer isKindOfClass:[ShapeFileLayer class]]) {
                CGPoint layerPoint = [[worldView layer] convertPoint:touchLocation toLayer:sublayer];
                ShapeFileLayer *shapeLayer = sublayer;
                if (CGPathContainsPoint(shapeLayer.path, 0, layerPoint, YES)) {
                    [self updateActive:shapeLayer];
                    return;
                }
            }
        }
    }
    
    [self updateActive:NULL];
    
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    isDragging = true;
    [self updateShapeLayer:touches];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    isDragging = false;
}

-(void)updateActive:(ShapeFileLayer *)new
{
    bool touchOther = (activeLayer != new);
    
    if((activeLayer && touchOther) || !isDragging)
    {
        ShapeFileLayer *disappearingLayer = activeLayer;
        //MUST be from toplayer.
        if(disappearingLayer.superlayer == dynamicView.layer)
        {
            [CATransaction begin];
            [CATransaction setAnimationDuration:0.5];
            [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
            [CATransaction setCompletionBlock:^{
                [disappearingLayer removeFromSuperlayer];
            }];
            [disappearingLayer setOpacity:0];
            [CATransaction commit];
        }
        activeLayer = nil;
    }
    
    if(new && touchOther)
    {
        NSString *name = new.shape.metadata[@"name"];
        NSLog(@"%@", name);
        
        //clone the layer.
        ShapeFileLayer *copyLayer = [[ShapeFileLayer alloc] initWithLayer:new];
        
        [copyLayer setOpacity:0];
        [copyLayer setIsHighlighted:YES];
        [dynamicView.layer addSublayer:copyLayer];
        
        [CATransaction begin];
        [CATransaction setAnimationDuration:0.5];
        [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [copyLayer setOpacity:1];
        [CATransaction commit];
        
        activeLayer =  copyLayer;
    }
}
@end
