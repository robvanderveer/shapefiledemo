//
//  shf.h
//  ShapeFileDemo
//
//  Created by Rob van der Veer on 4/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct
{
    double minX;
    double minY;
    double maxX;
    double maxY;
} shpBoundingBox;

typedef struct
{
    double x;
    double y;
} shpPoint;

typedef struct
{
    int recordNumber;
    shpBoundingBox boundingBox;
    int numParts;
    int numPoints;
    int *parts;
    shpPoint *points;
} shpShape;

@interface shp : NSObject
-(void)open:(NSString *)filename;
-(bool)eof;
-(shpShape)readShape;
-(void)freeShape:(shpShape)shape;
-(void)close;
-(shpBoundingBox)boundingBox;
@end
