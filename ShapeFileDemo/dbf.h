//
//  dbf.h
//  ShapeFileDemo
//
//  Created by Rob van der Veer on 4/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface dbf : NSObject
-(void)open:(NSString *)filename;
-(NSDictionary *)getRecord;
-(void)close;
@end
