//
//  ShapeFileReader.h
//  ShapeFileDemo
//
//  Created by Rob van der Veer on 4/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Shape : NSObject
@property (nonatomic) CGRect rect;
@property (nonatomic, strong) NSMutableArray *shapes;
@property (nonatomic, strong) NSDictionary *metadata;
@end

@interface ShapeFileReader : NSObject
@property (nonatomic, readonly) int shapeCount;
@property (nonatomic, readonly) CGRect rect;

-(ShapeFileReader *)initWithBaseName:(NSString *)filename;
-(Shape *)getShape:(int)index;
@end

