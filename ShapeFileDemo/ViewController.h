//
//  ViewController.h
//  ShapeFileDemo
//
//  Created by Rob van der Veer on 4/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShapeFileView.h"

@interface ViewController : UIViewController<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (strong, nonatomic) IBOutlet ShapeFileView *myMapView;
@property (strong, nonatomic) IBOutlet ShapeFileView *oldMapView;
@property (nonatomic) CGFloat zoomScale;
@end
