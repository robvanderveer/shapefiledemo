//
//  dbf.m
//  ShapeFileDemo
//
//  Created by Rob van der Veer on 4/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "dbf.h"

typedef struct
{
    uint8_t version;
    uint8_t yy;
    uint8_t mm;
    uint8_t dd;
    uint32_t numRecords;
    uint16_t headerLength;
    uint16_t recordLength;
    uint8_t reserved1;
    uint8_t reserved2;
    uint8_t incompleteTransaction;
    uint8_t encryptionFlag;
    uint8_t freeRecordThread[4];
    uint8_t multiUserDBase[8];
    uint8_t mdx;
    uint8_t language;
    uint8_t reserved3;
    uint8_t reserved4;
} dbfHeader;

typedef struct
{
    char name[11];
    uint8_t type;
    uint32_t dataAddress;
    uint8_t length;
    uint8_t decimalCount;
    uint8_t reserved18_19[2];
    uint8_t workAreaID;
    uint8_t reserved21_22[2];
    uint8_t flagsForSetFields;
    uint8_t reserved24_30[7];
    uint8_t indexField;
} dbfFieldSpecs;


@implementation dbf
//http://www.clicketyclick.dk/databases/xbase/format/dbf.html#DBF_STRUCT
//http://www.dbf2002.com/dbf-file-format.html
{
    NSInputStream *handle;
    uint8_t buffer[1024];
    dbfHeader header;
    int fieldSpecCount;
    dbfFieldSpecs fieldSpecs[100];  //nu ff geen tijd voor
    int recordCount;
    NSNumberFormatter *formatter;
}

-(void)open:(NSString *)filename
{
    handle = [[NSInputStream alloc] initWithFileAtPath:filename];
    [handle open];
    
    if(![handle hasBytesAvailable])
    {
        [NSException raise:@"Cannot read file" format:@"%@", filename];
    }
    
    NSInteger bytes = [handle read:buffer maxLength:sizeof(dbfHeader)];
    memcpy(&header, buffer, bytes);
    assert(bytes == 0x20);
    
    int numFields = 0;
    do {
        //load ahead.
        bytes = [handle read:buffer maxLength:1];
        //memcpy(&fieldSpec, buffer, bytes);
        if(buffer[0] == '\r')
        {
            break;
        }
        
        //load the definition.
        dbfFieldSpecs fieldSpec;
        bytes = [handle read:buffer+1 maxLength:sizeof(dbfFieldSpecs)-1];
        memcpy(&fieldSpec, buffer, bytes);
        assert(bytes+1 == 0x20);
        
        fieldSpecs[numFields++] = fieldSpec;
    } while (true);
    
    fieldSpecCount = numFields;
    
    formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setDecimalSeparator:@"."];
}

-(NSDictionary *)getRecord
{
    if(recordCount >= header.numRecords)
    {
        return NULL;
    }
    
    NSMutableDictionary *record = [[NSMutableDictionary alloc] init];
    NSInteger bytes;
    
    //read the entire record.
    uint8_t recordBuffer[header.recordLength];
    bytes = [handle read:recordBuffer maxLength:header.recordLength];
    if(bytes < header.recordLength)
    {
        return NULL;
    }
    
    //get the deleted flag at the end of the record.
    bool deleted;
    switch (recordBuffer[0]) {
        case 0x20:
            deleted = false;
            break;
        case 0x2A:
            deleted = true;
        default:
            [NSException raise:@"unexpected value" format:@"%c", buffer[0]];
            break;
    }
    
    //read the fields.
    int offset = 1;
    for(int fieldNum = 0; fieldNum < fieldSpecCount; fieldNum++)
    {
        dbfFieldSpecs spec = fieldSpecs[fieldNum];
        
        memcpy(buffer, recordBuffer + offset, spec.length);
        offset += spec.length;
        
        NSString *fieldName = [[NSString alloc] initWithUTF8String:spec.name];
        if([fieldName isEqualToString:@"name"] == false)
        {
            //save memory by skipping everything but the name.
            continue;
        }
        
        NSString *trimmedString = [self getTerminatedString:buffer maxLength:spec.length trimSpaces:TRUE];

        switch(spec.type)
        {
            case 'C':
            {
                [record setValue:trimmedString forKey:fieldName];
            }
                break;
            case 'N':
            {
                //NSNumber *myNumber = [formatter numberFromString:value];
                //[record setObject:myNumber forKey:fieldName];
            }
                break;
            default:
                [NSException raise:@"Unknown field type" format:@"type %c", spec.type];
        }
    }
    
    recordCount++;
    
    return record;
}

-(NSString *)getTerminatedString:(uint8_t *)buf maxLength:(int)maxLength trimSpaces:(bool)trimSpaces
{
    int len = maxLength;
    
    while(len > 0 && buffer[len-1] == '\0')
    {
        len++;
    }
    
    if(trimSpaces)
    {
        //find the last ' ';
        while(len > 0 && buffer[len-1] == ' ')
        {
            len--;
        }
    }
    if(len == 0)
    {
        return @"";
    }
    
    NSString *value = [[NSString alloc] initWithBytes:buf length:len  encoding:NSUTF8StringEncoding];
    return value;
    
}

-(void)close
{
    [handle close];
    handle = nil;
}
@end
