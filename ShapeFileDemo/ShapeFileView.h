//
//  ShapeFileView.h
//  ShapeFileDemo
//
//  Created by Rob van der Veer on 5/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShapeFileReader.h"
#import "ShapeFileLayer.h"
#import <QuartzCore/QuartzCore.h>

@interface ShapeFileView : UIView
@property (nonatomic, strong) UIColor *shapeBorderColor;
@property (nonatomic, strong) ShapeFileReader *reader;
-(id)initWithFrame:(CGRect)frame andReader:(ShapeFileReader *)reader;
-(ShapeFileLayer *)getLayerForCountry:(NSString *)countryName;
-(void)rebuild;
@end
