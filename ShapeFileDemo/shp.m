//
//  shf.m
//  ShapeFileDemo
//
//  Created by Rob van der Veer on 4/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "shp.h"

@interface shp()
{
    NSData *file;
    long position;
    int shapeType;
    int fileLength;
    int version;
    shpBoundingBox box;
}
@end

@implementation shp

-(void)open:(NSString *)filename
{
    file = [[NSData alloc] initWithContentsOfFile:filename];
    NSLog(@"Opened %@, %d bytes", filename, file.length);
    if(file.length == 0)
    {
        [NSException raise:@"Failed to open file" format:@"filename %@", filename];
    }
    
    //http://en.wikipedia.org/wiki/Shapefile#Shapefile_shape_format_.28.shp.29
    //better:
    //http://www.esri.com/library/whitepapers/pdfs/shapefile.pdf
    
    //verify code.
    int32_t code = [self readBigEndianInt];
    if(code != 0x0000270a)
    {
        [NSException raise:@"bad format" format:@"%xd", code];
    }
    // 5 unused 32bits
    [self readBigEndianInt];
    [self readBigEndianInt];
    [self readBigEndianInt];
    [self readBigEndianInt];
    [self readBigEndianInt];
    
    fileLength = [self readBigEndianInt];
    version = [self readInt];
    shapeType = [self readInt];
    assert(position == 36);
    
    box = [self readBoundingBox];
    double minZ = [self readDouble];
    double maxZ = [self readDouble];
    double minM = [self readDouble];
    double maxM = [self readDouble];
    minZ = maxZ = minM = maxM = 0;
    
    assert(position == 100);
}

-(bool)eof
{
    if(position >= fileLength*2)
    {
        return true;
    }
    return false;
}

-(shpBoundingBox)boundingBox
{
    return box;
}

-(shpShape)readShape
{
    if([self eof])
    {
        [NSException raise:@"Attempt to read past end of file" format:@""];
    }
    
    shpShape returnShape;
    
    returnShape.recordNumber = [self readBigEndianInt];
    
    int recordLength = [self readBigEndianInt];
    long rememberPositionToAssert = position;
    
    int shapeType2 = [self readInt];
    
    switch(shapeType2)
    {
        case 0:
        {
            long nullType = [self readInt];
            assert(nullType == 0);
            break;
        }
        case 3:
        case 5:       //polyline, polygon
                      //MBR, numparts, numpoints, parts, points.
        {
            returnShape.boundingBox = [self readBoundingBox];
            returnShape.numParts = [self readInt];
            returnShape.numPoints = [self readInt];
            returnShape.parts = malloc(sizeof(int) * returnShape.numParts);
            returnShape.points = malloc(sizeof(shpPoint) * returnShape.numPoints);
            
            for(int i = 0; i < returnShape.numParts; i++)
            {
                returnShape.parts[i] = [self readInt];
            }
            
            for(int i = 0; i < returnShape.numPoints; i++)
            {
                returnShape.points[i].x = [self readDouble];
                returnShape.points[i].y = [self readDouble];
            }
        }
            break;
        default:
            [NSException raise:@"Unsupported shape" format:@"type %d", shapeType];
            break;
    }
    
    assert(position - rememberPositionToAssert == recordLength * 2);
    return returnShape;
}

-(void)freeShape:(shpShape)shape
{
    free(shape.parts);
    free(shape.points);
}

-(void)close
{
    file = nil;
}

#pragma mark Helpers

-(int)readBigEndianInt
{
    int buf;
    [file getBytes:&buf range:NSMakeRange(position, 4)];
    position += 4;
    
    int num = NSSwapBigIntToHost(buf);
    return num;
}

-(int)readInt
{
    int buf;
    [file getBytes:&buf range:NSMakeRange(position, 4)];
    position += 4;
    return buf;
}

-(double)readDouble
{
    double buf;
    [file getBytes:&buf range:NSMakeRange(position, 8)];
    position += 8;
    
    return buf;
}

-(shpBoundingBox)readBoundingBox
{
    shpBoundingBox buf;
    [file getBytes:&buf range:NSMakeRange(position, sizeof(shpBoundingBox))];
    position += sizeof(shpBoundingBox);
    
    return buf;
}

@end
